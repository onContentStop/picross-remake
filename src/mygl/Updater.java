package mygl;

/**
 * Created by kyle on 6/16/17.
 */
public interface Updater {
    public void update();
}
